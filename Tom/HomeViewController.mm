//
//  HomeViewController.m
//  Pokemon
//
//  Created by 威 王 on 12-7-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#include "Dirac.h"
#include <stdio.h>
#include <sys/time.h>
#import "EAFRead.h"
#import "EAFWrite.h"

double gExecTimeTotal = 0.;
void DeallocateAudioBuffer(float **audio, int numChannels)
{
	if (!audio) return;
	for (long v = 0; v < numChannels; v++) {
		if (audio[v]) {
			free(audio[v]);
			audio[v] = NULL;
		}
	}
	free(audio);
	audio = NULL;
}

float **AllocateAudioBuffer(int numChannels, int numFrames)
{
	// Allocate buffer for output
	float **audio = (float**)malloc(numChannels*sizeof(float*));
	if (!audio) return NULL;
	memset(audio, 0, numChannels*sizeof(float*));
	for (long v = 0; v < numChannels; v++) {
		audio[v] = (float*)malloc(numFrames*sizeof(float));
		if (!audio[v]) {
			DeallocateAudioBuffer(audio, numChannels);
			return NULL;
		}
		else memset(audio[v], 0, numFrames*sizeof(float));
	}
	return audio;
}	


long myReadData(float **chdata, long numFrames, void *userData)
{	
	if (!chdata)	return 0;
	
	HomeViewController *Self = (HomeViewController*)userData;
	if (!Self)	return 0;
	
	gExecTimeTotal += DiracClockTimeSeconds(); 		
    
	OSStatus err = [Self.reader readFloatsConsecutive:numFrames intoArray:chdata];
	
	DiracStartClock();								
	return err;
	
}

@interface HomeViewController ()
- (void)record;
- (void)stopRecord;
- (void)play;
- (BOOL)canRecord;
- (void)updateMeters:(NSTimer *)sender;
- (void)mackChangeSound;
@end

@implementation HomeViewController

- (void)dealloc
{
    [super dealloc];
	[_audioSession release];
    [_player release];
    [_recorder release];
	[_timer release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *title = @"山寨汤姆猫";
    CGSize titleSize = [title sizeWithFont:[UIFont boldSystemFontOfSize:20.0f]];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((320-titleSize.width)/2, 10, titleSize.width, titleSize.height)];
    titleLabel.text = title;
    titleLabel.font = [UIFont boldSystemFontOfSize:20.0f];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [self.view addSubview:titleLabel];
    [titleLabel release];
    
    UIButton *cymbalButton = [[UIButton alloc] initWithFrame:CGRectMake(2, 300, 50, 50)];
    [cymbalButton setBackgroundImage:[UIImage imageNamed:@"btn_cat_print-60@2x.png"] forState:UIControlStateNormal];
    [cymbalButton addTarget:self action:@selector(cymbalButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cymbalButton];
    [cymbalButton release];
    
    UIButton *scratchButton = [[UIButton alloc] initWithFrame:CGRectMake(2, 350, 50, 50)];
    [scratchButton setBackgroundImage:[UIImage imageNamed:@"btn_cat_print-60@2x.png"] forState:UIControlStateNormal];
    [scratchButton addTarget:self action:@selector(scratchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scratchButton];
    [scratchButton release];
    
    UIButton *milkButton = [[UIButton alloc] initWithFrame:CGRectMake(2, 400, 50, 50)];
    [milkButton setBackgroundImage:[UIImage imageNamed:@"gumb-milk-60@2x.png"] forState:UIControlStateNormal];
    [milkButton addTarget:self action:@selector(milkButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:milkButton];
    [milkButton release];
    
    UIButton *birdButton = [[UIButton alloc] initWithFrame:CGRectMake(320-50, 300, 50, 50)];
    [birdButton setBackgroundImage:[UIImage imageNamed:@"gumb-bird2-60@2x.png"] forState:UIControlStateNormal];
    [birdButton addTarget:self action:@selector(birdButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:birdButton];
    [birdButton release];
    
    UIButton *pieButton = [[UIButton alloc] initWithFrame:CGRectMake(320-50, 350, 50, 50)];
    [pieButton setBackgroundImage:[UIImage imageNamed:@"gumb-bird2-60@2x.png"] forState:UIControlStateNormal];
    [pieButton addTarget:self action:@selector(pieButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:pieButton];
    [pieButton release];
    
    UIButton *recordButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
    [recordButton setBackgroundImage:[UIImage imageNamed:@"gumb-milk-60@2x.png"] forState:UIControlStateNormal];
    [recordButton addTarget:self action:@selector(startRecord:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:recordButton];
    [recordButton release];
    
    UIButton *stopRecordButton = [[UIButton alloc] initWithFrame:CGRectMake(60, 10, 50, 50)];
    [stopRecordButton setBackgroundImage:[UIImage imageNamed:@"gumb-bird2-60@2x.png"] forState:UIControlStateNormal];
    [stopRecordButton addTarget:self action:@selector(stopRecord:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:stopRecordButton];
    [stopRecordButton release];
    
    UIButton *playButton = [[UIButton alloc] initWithFrame:CGRectMake(110, 10, 50, 50)];
    [playButton setBackgroundImage:[UIImage imageNamed:@"gumb-bird2-60@2x.png"] forState:UIControlStateNormal];
    [playButton addTarget:self action:@selector(playRecord:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
    [playButton release];
    
    [self performSelector:@selector(startYawn) withObject:nil afterDelay:0.5];
	
	_lowTimes = 0;
	_playTimes = 0;
	
	//设置路径
    NSArray *searchPaths =  NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    audioPath = [[searchPaths objectAtIndex:0] stringByAppendingPathComponent:@"audio.wav"];
    outUrl = [[NSURL alloc] initFileURLWithPath:[[searchPaths objectAtIndex:0] stringByAppendingPathComponent:@"audio.aif"]];
	
    [audioPath retain];
	
	[self record];
    
    //启用监听
	[[SCListener sharedListener] listen];
	_timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateMeters:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
}

- (void)milkButtonClick:(id)sender
{
    [self initDrinkArray];
    [self startAnimationWithImages:drinkArray duration:10];
    [self performSelector:@selector(playSoundWithFile:) withObject:@"pour_milk" afterDelay:2.4];
    [self performSelector:@selector(playSoundWithFile:) withObject:@"p_drink_milk" afterDelay:5.8];
}

- (void)scratchButtonClick:(id)sender
{
    [self initScratchArray];
    [self startAnimationWithImages:scratchArray duration:8];
    [self performSelector:@selector(playSoundWithFile:) withObject:@"scratch_kratzen" afterDelay:3.8];
}

- (void)cymbalButtonClick:(id)sender
{
    [self initCymbalArray];
    [self startAnimationWithImages:cymbalArray duration:2];
    [self performSelector:@selector(playSoundWithFile:) withObject:@"cymbal" afterDelay:0.8];
}

- (void)birdButtonClick:(id)sender
{
    [self initBirdArray];
    [self startAnimationWithImages:birdArray duration:4];
    [self performSelector:@selector(playSoundWithFile:) withObject:@"p_eat" afterDelay:1.8];
}

- (void)pieButtonClick:(id)sender
{
    [self initPieArray];
    [self startAnimationWithImages:pieArray duration:4];
}

int touchHeader = 0;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{ 
    UITouch  *touch = [[event touchesForView:self.view] anyObject]; 
    CGPoint location = [touch locationInView:gifImageView]; 
    int number = [self getRandomNumber:1 to:2];
    if ((location.x >= 160.0 && location.y >= 440.0) && (location.x <= 195.0 && location.y <= 460.0)) {
        [self initFootLeftArray];
        [self startAnimationWithImages:footLeftArray duration:3];
        [self playSoundWithFile:number == 1 ? @"p_foot3" : @"p_foot4"];
    }
    if ((location.x >= 122.0 && location.y >= 440.0) && (location.x <= 155.0 && location.y <= 460.0)) {
        [self initFootRightArray];
        [self startAnimationWithImages:footRightArray duration:3];
        [self playSoundWithFile:number == 1 ? @"p_foot3" : @"p_foot4"];
    }
    if ((location.x >= 116.0 && location.y >= 298.0) && (location.x <= 204.0 && location.y <= 419.0)) {
        [self initStomachArray];
        [self startAnimationWithImages:stomachArray duration:3];
        [self playSoundWithFile:number == 1 ? @"p_belly1" : @"p_belly2"];
    }
    if ((location.x >= 91.0 && location.y >= 112.0) && (location.x <= 213.0 && location.y <= 256.0)) {
        touchHeader++;
        NSString *file = [NSString stringWithFormat:@"slap%d",[self getRandomNumber:1 to:6]];
        
        if (touchHeader >= 3) {
            [self initKnockout2Array];
            [self startAnimationWithImages:knockout2Array duration:10];
            [self playSoundWithFile:file];
            [self performSelector:@selector(playSoundWithFile:) withObject:@"fall" afterDelay:1.8];
            [self performSelector:@selector(playSoundWithFile:) withObject:@"p_stars2s" afterDelay:2.3];
            touchHeader = 0;
        }else{
            [self initKnockout1Array];
            [self startAnimationWithImages:knockout1Array duration:2];
            [self playSoundWithFile:file];
        }
    }
    
    NSLog(@"right touch %f,%f",location.x,location.y); 
} 

-(int)getRandomNumber:(int)from to:(int)to {
    return (int)(from + arc4random() % (to-from+1));
}

@synthesize audioSession = _audioSession;
@synthesize reader = _reader;
- (void)record {
    
    if ([self canRecord]) {
        NSError *error;
        
        NSMutableDictionary *_recordSetting = [[NSMutableDictionary alloc] init];
        
        [_recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [_recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [_recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
		[_recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityMax] forKey:AVEncoderAudioQualityKey];
        
        //_recorderTmpFile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithString:@"recording.caf"]]];
		
		//[_recorderTmpFile retain];
		
		if (_recorder == NULL) {
			_recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:audioPath] settings:_recordSetting error:&error];
		}else {
			[_recorder release];
			_recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:audioPath] settings:_recordSetting error:&error];
		}
        
		
		[_recorder setMeteringEnabled:YES];
		[_recorder setDelegate:self];
        [_recorder prepareToRecord];
        // [_recorder record];
		
		// NSLog(@"录制中～～～～～～～～～～～～～！！！！");
		[_recordSetting release];
    }else {
        NSLog(@"不能录制！！！！");
    }
	
}

- (void)updateMeters:(NSTimer *)sender {
    
	if ([[SCListener sharedListener] averagePower] > 0.1) {
		
		if (!ISRECORDED) {
			if (!isRecording) {
                [self startListen];
				NSLog(@"1开始录制");
				isRecording = YES;
				
				ISRECORDED = YES;  //记录是否录音过声音
				_playTimes = 0;
				
				//[self record];
				[_recorder prepareToRecord];
				[_recorder record];
			}
			
		}else {
			if (!isPlaying) {
				if (!isRecording) {
                    [self startListen];
					NSLog(@"2开始录制");
					isRecording = YES;
					_playTimes = 0;
					[_recorder prepareToRecord];
					[_recorder record];
					//[self record];
				}
				
			}
		}
	}
	
	if ([[SCListener sharedListener] averagePower] < 0.1) {
		
		_lowTimes++;
		
		if (_lowTimes >= 5) {  //5  次  0.5s=======
			_lowTimes = 0;
			isRecording = NO;
			if (!isRecording & !isPlaying) {
				
				if (ISRECORDED) {
					
					if (_playTimes < 1) {
						_playTimes++;
						
						NSLog(@"停止录制内容");
						[self stopRecord];
						isPlaying = YES;
					}
					
				}
			}
		}
	}
}

- (void)stopRecord {
    [_recorder stop];
}

- (void)mackChangeSound {
    //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	_reader = [[EAFRead alloc] init];
	_writer = [[EAFWrite alloc] init];
	
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~xxxxx");
	long numChannels = 1;		// DIRAC LE allows mono only
	float sampleRate = 44100.;
    
	NSLog(@"inurl = %@;outurl = %@", inUrl, outUrl);
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1");
	[_reader openFileForRead:[NSURL fileURLWithPath:audioPath] sr:sampleRate channels:numChannels];
	NSLog(@"!!!!!~~~~~~!!!!!!!!!!!~~~~~~~~");
	[_writer openFileForWrite:outUrl sr:sampleRate channels:numChannels wordLength:16 type:kAudioFileAIFFType];
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~2");
	float time      = 1.0;                
	float pitch     = 1.4;
	float formant   = 1.0; 
	
	void *dirac = DiracCreate(kDiracLambdaPreview, kDiracQualityPreview, numChannels, sampleRate, &myReadData, (void*)self);
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~3");
	//if (!dirac) {
    //		printf("!! ERROR !!\n\n\tCould not create DIRAC instance\n\tCheck number of channels and sample rate!\n");
    //		printf("\n\tNote that the free DIRAC LE library supports only\n\tone channel per instance\n\n\n");
    //		exit(-1);
    //	}
	
	DiracSetProperty(kDiracPropertyTimeFactor, time, dirac);
	DiracSetProperty(kDiracPropertyPitchFactor, pitch, dirac);
	DiracSetProperty(kDiracPropertyFormantFactor, formant, dirac);
    
	if (pitch > 1.0)
		DiracSetProperty(kDiracPropertyUseConstantCpuPitchShift, 1, dirac);
    
	DiracPrintSettings(dirac);
	
	//NSLog(@"Running DIRAC version %s\nStarting processing", DiracVersion());
	
	SInt64 numf = [_reader fileNumFrames];
	SInt64 outframes = 0;
	SInt64 newOutframe = numf*time;
	//long lastPercent = -1;
	percent = 0;
	
	long numFrames = 8192;
	
	float **audio = AllocateAudioBuffer(numChannels, numFrames);
    
	double bavg = 0;
	
	for(;;) {
		
		//percent = 100.f*(double)outframes / (double)newOutframe;
        //		long ipercent = percent;
        //        
        //		if (lastPercent != percent) 
        //        {
        //			//[self performSelectorOnMainThread:@selector(updateBarOnMainThread:) withObject:self waitUntilDone:NO];
        //			//printf("\rProgress: %3i%% [%-40s] ", ipercent, &"||||||||||||||||||||||||||||||||||||||||"[40 - ((ipercent>100)?40:(2*ipercent/5))] );
        //			lastPercent = ipercent;
        //			fflush(stdout);
        //		}
		
		DiracStartClock();								
		long ret = DiracProcess(audio, numFrames, dirac);
		bavg += (numFrames/sampleRate);
		gExecTimeTotal += DiracClockTimeSeconds();				
		//printf("x realtime = %3.3f : 1 (DSP only), CPU load (peak, DSP+disk): %3.2f%%\n", bavg/gExecTimeTotal, DiracPeakCpuUsagePercent(dirac));
		
		long framesToWrite = numFrames;
		unsigned long nextWrite = outframes + numFrames;
		if (nextWrite > newOutframe) framesToWrite = numFrames - nextWrite + newOutframe;
		if (framesToWrite < 0) framesToWrite = 0;
		
		[_writer writeFloats:framesToWrite fromArray:audio];
		outframes += numFrames;
		
		if (ret <= 0)
            break;
	}
	
	//percent = 100;
	//[self performSelectorOnMainThread:@selector(updateBarOnMainThread:) withObject:self waitUntilDone:NO];
    
	DeallocateAudioBuffer(audio, numChannels);
	DiracDestroy( dirac );
	
	[_reader release];
	[_writer release];
	// Done!
	NSLog(@"\nDone!");
	//
	[self play];
	//[pool release];
	
	
}

- (void)play {
	NSLog(@"播放录制内容");
    NSError *error;
    // NSString *_filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithString:@"out.aif"]] retain];
    
    if (_player == NULL) {
        
    }else {
        [_player release];
    }
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:outUrl error:&error];
	_player.delegate = self;
    [_player play];
}


- (BOOL)canRecord {
    NSError *error;
    self.audioSession = [AVAudioSession sharedInstance];
    
    if (![self.audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error]) {
        NSLog(@"Error %@", [error localizedDescription]);
        return  NO;
    }
    
    if (![self.audioSession setActive:YES error:&error]) {
		NSLog(@"Error %@", [error localizedDescription]);
        return  NO;
    }
    
    return self.audioSession.inputIsAvailable;
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
	
    NSLog(@"录音结束并且成功  ～～～～～@@～～～～～～");
	//[self play];
	NSData *data = [NSData dataWithContentsOfURL:_recorder.url];
    [data writeToFile:audioPath atomically:YES];

    [self mackChangeSound];
    [self stopListen];
    [self initTalkArray];
    [self startAnimationWithImages:talkArray duration:0.12 repeatCount:3];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error {
    NSLog(@"录音失败");
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    NSLog(@"播放结束！！！～～～@@～～～～");
	isPlaying = NO;
    [self stopListen];
    [[SCListener sharedListener] listen];
}

@end
