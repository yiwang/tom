//
//  BaseViewController.m
//  Pokemon
//
//  Created by 威 王 on 12-7-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BaseViewController.h"

@implementation BaseViewController

- (void)dealloc
{
    [super dealloc];
    [yawnArray release];
    [drinkArray release];
    [footLeftArray release];
    [footRightArray release];
    [stomachArray release];
    [knockout1Array release];
    [knockout2Array release];
    [scratchArray release];
    [cymbalArray release];
    [birdArray release];
    [pieArray release];
    [gifImageView release];
    [talkArray release];
    [player release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initGifImageView];
}

- (void)initGifImageView
{
    gifImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    gifImageView.image = [UIImage imageNamed:@"cat_zeh0030.jpg"];
    [self.view addSubview:gifImageView];
}

- (void)startYawn
{
    [self initYawnArray];
    [self startAnimationWithImages:yawnArray duration:3];
    [self playSoundWithFile:@"p_yawn3_11a"];
}

- (void)startListen
{
    gifImageView.image = [UIImage imageNamed:@"cat_listen.jpg"];
}

- (void)stopListen
{
    gifImageView.image = [UIImage imageNamed:@"cat_zeh0030.jpg"];
}

- (void)startAnimationWithImages:(NSMutableArray*)images duration:(NSTimeInterval)duration
{
    [self startAnimationWithImages:images duration:duration repeatCount:1];
}

- (void)startAnimationWithImages:(NSMutableArray*)images duration:(NSTimeInterval)duration repeatCount:(int)repeatCount
{
    if (gifImageView.isAnimating) {
        [gifImageView stopAnimating];
    }
    if (player) {
        [player stop];
    }
    [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(playSoundWithFile:) object:@"pour_milk"];
    [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(playSoundWithFile:) object:@"p_drink_milk"];
    [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(playSoundWithFile:) object:@"fall"];
    [NSThread cancelPreviousPerformRequestsWithTarget:self selector:@selector(playSoundWithFile:) object:@"p_stars2s"];
    
    gifImageView.animationImages = nil;
    
    gifImageView.animationImages = images; //动画图片数组
    gifImageView.animationDuration = duration; //执行一次完整动画所需的时长
    gifImageView.animationRepeatCount = repeatCount;  //动画重复次数
    [gifImageView startAnimating];
}

-(void)playSoundWithFile:(NSString*)filename
{
    [[SCListener sharedListener] stop];
    
    NSString *outputSound = [[NSBundle mainBundle] pathForResource:filename ofType:@"wav"];
	outUrlB = [NSURL fileURLWithPath:outputSound];
    
	NSError *error = nil;
	player = [[AVAudioPlayer alloc] initWithContentsOfURL:outUrlB error:&error];
	if (error)
		NSLog(@"AVAudioPlayer error %@, %@", error, [error userInfo]);
    
	[player play];
    player.delegate = self;
}

- (void)clearWithArray:(NSMutableArray *)array
{
    [array removeAllObjects];
}

- (void)initDrinkArray
{
    if (!drinkArray) {
        drinkArray = [[NSMutableArray arrayWithCapacity:81] retain];
        for (int i = 0; i < 81; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            NSString *imageName = [NSString stringWithFormat:@"cat_drink00%@.jpg",count];
            UIImage *image = [UIImage imageNamed:imageName];
            
            [drinkArray addObject:image];
        }
    }
}

- (void)initTalkArray
{
    if (!talkArray) {
        talkArray = [[NSMutableArray arrayWithCapacity:15] retain];
        for (int i = 0; i < 15; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            NSString *imageName = [NSString stringWithFormat:@"cat_talk00%@.jpg",count];
            UIImage *image = [UIImage imageNamed:imageName];
            
            [talkArray addObject:image];
        }
    }
}

- (void)initYawnArray
{
    if (!yawnArray) {
        yawnArray = [[NSMutableArray arrayWithCapacity:31] retain];
        for (int i = 0; i < 31; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"cat_zeh00%@.jpg",count];
            UIImage *image = [UIImage imageNamed:imageName];
            [yawnArray addObject:image];
        }
    }
}

- (void)initFootLeftArray
{
    if (!footLeftArray) {
        footLeftArray = [[NSMutableArray arrayWithCapacity:30] retain];
        for (int i = 0; i < 30; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            NSString *imageName = [NSString stringWithFormat:@"cat_foot_left00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [footLeftArray addObject:image];
        }
    }
}

- (void)initFootRightArray
{
    if (!footRightArray) {
        footRightArray = [[NSMutableArray arrayWithCapacity:30] retain];
        for (int i = 0; i < 30; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            NSString *imageName = [NSString stringWithFormat:@"cat_foot_right00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [footRightArray addObject:image];
        }
    }
}

- (void)initStomachArray
{
    if (!stomachArray) {
        stomachArray = [[NSMutableArray arrayWithCapacity:34] retain];
        for (int i = 0; i < 33; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            NSString *imageName = [NSString stringWithFormat:@"cat_stomach00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [stomachArray addObject:image];
        }
    }
}

- (void)initKnockout1Array
{
    if (!knockout1Array) {
        knockout1Array = [[NSMutableArray arrayWithCapacity:10] retain];
        for (int i = 0; i < 10; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"cat_knockout00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [knockout1Array addObject:image];
        }
    }
}

- (void)initKnockout2Array
{
    if (!knockout2Array) {
        knockout2Array = [[NSMutableArray arrayWithCapacity:81] retain];
        for (int i = 0; i < 81; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"cat_knockout00%@.jpg",count];
            UIImage *image = [UIImage imageNamed:imageName];
            [knockout2Array addObject:image];
        }
    }
}

- (void)initScratchArray
{
    if (!scratchArray) {
        scratchArray = [[NSMutableArray arrayWithCapacity:56] retain];
        for (int i = 0; i < 56; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"Scratch00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [scratchArray addObject:image];
        }
    }
}

- (void)initCymbalArray
{
    if (!cymbalArray) {
        cymbalArray = [[NSMutableArray arrayWithCapacity:13] retain];
        for (int i = 0; i < 13; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"cat_cymbal00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [cymbalArray addObject:image];
        }
    }
}

- (void)initBirdArray
{
    if (!birdArray) {
        birdArray = [[NSMutableArray arrayWithCapacity:40] retain];
        for (int i = 0; i < 40; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"cat_eat00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            
            [birdArray addObject:image];
        }
    }
}

- (void)initPieArray
{
    if (!pieArray) {
        pieArray = [[NSMutableArray arrayWithCapacity:23] retain];
        for (int i = 0; i < 23; i++) {
            NSString *count = 0;
            if (i < 10) {
                count = [NSString stringWithFormat:@"0%d",i];
            }else{
                count = [NSString stringWithFormat:@"%d",i];
            }
            
            NSString *imageName = [NSString stringWithFormat:@"pie00%@.jpg",count];
            //            NSString *resource = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
            UIImage *image = [UIImage imageNamed:imageName];
            [pieArray addObject:image];
        }
    }
}

@end
