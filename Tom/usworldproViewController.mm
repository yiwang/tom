//
//  usworldproViewController.m
//  usworldpro
//
//  Created by 石 on 12-7-24.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "usworldproViewController.h"
#include "Dirac.h"
#include <stdio.h>
#include <sys/time.h>
#import "EAFRead.h"
#import "EAFWrite.h"


double gExecTimeTotal = 0.;
void DeallocateAudioBuffer(float **audio, int numChannels)
{
	if (!audio) return;
	for (long v = 0; v < numChannels; v++) {
		if (audio[v]) {
			free(audio[v]);
			audio[v] = NULL;
		}
	}
	free(audio);
	audio = NULL;
}

float **AllocateAudioBuffer(int numChannels, int numFrames)
{
	// Allocate buffer for output
	float **audio = (float**)malloc(numChannels*sizeof(float*));
	if (!audio) return NULL;
	memset(audio, 0, numChannels*sizeof(float*));
	for (long v = 0; v < numChannels; v++) {
		audio[v] = (float*)malloc(numFrames*sizeof(float));
		if (!audio[v]) {
			DeallocateAudioBuffer(audio, numChannels);
			return NULL;
		}
		else memset(audio[v], 0, numFrames*sizeof(float));
	}
	return audio;
}	


long myReadData(float **chdata, long numFrames, void *userData)
{	
	if (!chdata)	return 0;
	
	usworldproViewController *Self = (usworldproViewController*)userData;
	if (!Self)	return 0;
	
	gExecTimeTotal += DiracClockTimeSeconds(); 		
    
	OSStatus err = [Self.reader readFloatsConsecutive:numFrames intoArray:chdata];
	
	DiracStartClock();								
	return err;
	
}

@interface usworldproViewController ()
- (void)record;
- (void)stopRecord;
- (void)play;
- (BOOL)canRecord;
- (void)updateMeters:(NSTimer *)sender;
- (void)mackChangeSound;
@end

@implementation usworldproViewController
@synthesize audioSession = _audioSession;
@synthesize reader = _reader;
- (void)record {
    
    if ([self canRecord]) {
        NSError *error;
        
        NSMutableDictionary *_recordSetting = [[NSMutableDictionary alloc] init];
        
        [_recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [_recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
        [_recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
		[_recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityMax] forKey:AVEncoderAudioQualityKey];
        
        //_recorderTmpFile = [NSURL fileURLWithPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithString:@"recording.caf"]]];
		
		//[_recorderTmpFile retain];
		
		if (_recorder == NULL) {
			_recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:audioPath] settings:_recordSetting error:&error];
		}else {
			[_recorder release];
			_recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:audioPath] settings:_recordSetting error:&error];
		}

		
		[_recorder setMeteringEnabled:YES];
		[_recorder setDelegate:self];
        [_recorder prepareToRecord];
       // [_recorder record];
		
		// NSLog(@"录制中～～～～～～～～～～～～～！！！！");
		[_recordSetting release];
    }else {
        NSLog(@"不能录制！！！！");
    }
	
}

- (void)updateMeters:(NSTimer *)sender {
	//NSLog(@"～～～～～～～～～～～～～～～～～监听中!!!~~~~~");
	//NSLog(@"AVG = %f;peak = %f", [[SCListener sharedListener] averagePower], [[SCListener sharedListener] peakPower]);
	
	if ([[SCListener sharedListener] averagePower] > 0.1) {
		
		if (!ISRECORDED) {
			if (!isRecording) {
				NSLog(@"1开始录制");
				isRecording = YES;
				
				ISRECORDED = YES;  //记录是否录音过声音
				_playTimes = 0;
				
				//[self record];
				[_recorder prepareToRecord];
				[_recorder record];
			}
			
		}else {
			if (!isPlaying) {
				if (!isRecording) {
					NSLog(@"2开始录制");
					isRecording = YES;
					_playTimes = 0;
					[_recorder prepareToRecord];
					[_recorder record];
					//[self record];
				}
				
			}
		}

		
	}
	
	if ([[SCListener sharedListener] averagePower] < 0.1) {
		
		_lowTimes++;
		
		if (_lowTimes >= 5) {  //5  次  0.5s=======
			_lowTimes = 0;
			isRecording = NO;
			if (!isRecording & !isPlaying) {
				
				if (ISRECORDED) {
					
					if (_playTimes < 1) {
						_playTimes++;
						
						NSLog(@"停止录制内容");
						[self stopRecord];
						isPlaying = YES;
					}
					
				}

				
			}
			
		}
	}
}

- (void)stopRecord {
    [_recorder stop];
}

- (void)mackChangeSound {
    //NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	_reader = [[EAFRead alloc] init];
	_writer = [[EAFWrite alloc] init];
	
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~xxxxx");
	long numChannels = 1;		// DIRAC LE allows mono only
	float sampleRate = 44100.;
	  
	NSLog(@"inurl = %@;outurl = %@", inUrl, outUrl);
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1");
	[_reader openFileForRead:[NSURL fileURLWithPath:audioPath] sr:sampleRate channels:numChannels];
	NSLog(@"!!!!!~~~~~~!!!!!!!!!!!~~~~~~~~");
	[_writer openFileForWrite:outUrl sr:sampleRate channels:numChannels wordLength:16 type:kAudioFileAIFFType];
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~2");
	float time      = 1.0;                
	float pitch     = 1.4;
	float formant   = 1.0; 
	
	void *dirac = DiracCreate(kDiracLambdaPreview, kDiracQualityPreview, numChannels, sampleRate, &myReadData, (void*)self);
	NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~3");
	//if (!dirac) {
//		printf("!! ERROR !!\n\n\tCould not create DIRAC instance\n\tCheck number of channels and sample rate!\n");
//		printf("\n\tNote that the free DIRAC LE library supports only\n\tone channel per instance\n\n\n");
//		exit(-1);
//	}
	
	DiracSetProperty(kDiracPropertyTimeFactor, time, dirac);
	DiracSetProperty(kDiracPropertyPitchFactor, pitch, dirac);
	DiracSetProperty(kDiracPropertyFormantFactor, formant, dirac);

	if (pitch > 1.0)
		DiracSetProperty(kDiracPropertyUseConstantCpuPitchShift, 1, dirac);
    
	DiracPrintSettings(dirac);
	
	//NSLog(@"Running DIRAC version %s\nStarting processing", DiracVersion());
	
	SInt64 numf = [_reader fileNumFrames];
	SInt64 outframes = 0;
	SInt64 newOutframe = numf*time;
	//long lastPercent = -1;
	percent = 0;
	
	long numFrames = 8192;
	
	float **audio = AllocateAudioBuffer(numChannels, numFrames);
    
	double bavg = 0;
	
	for(;;) {
		
		//percent = 100.f*(double)outframes / (double)newOutframe;
//		long ipercent = percent;
//        
//		if (lastPercent != percent) 
//        {
//			//[self performSelectorOnMainThread:@selector(updateBarOnMainThread:) withObject:self waitUntilDone:NO];
//			//printf("\rProgress: %3i%% [%-40s] ", ipercent, &"||||||||||||||||||||||||||||||||||||||||"[40 - ((ipercent>100)?40:(2*ipercent/5))] );
//			lastPercent = ipercent;
//			fflush(stdout);
//		}
		
		DiracStartClock();								
		long ret = DiracProcess(audio, numFrames, dirac);
		bavg += (numFrames/sampleRate);
		gExecTimeTotal += DiracClockTimeSeconds();				
		//printf("x realtime = %3.3f : 1 (DSP only), CPU load (peak, DSP+disk): %3.2f%%\n", bavg/gExecTimeTotal, DiracPeakCpuUsagePercent(dirac));
		
		long framesToWrite = numFrames;
		unsigned long nextWrite = outframes + numFrames;
		if (nextWrite > newOutframe) framesToWrite = numFrames - nextWrite + newOutframe;
		if (framesToWrite < 0) framesToWrite = 0;
		
		[_writer writeFloats:framesToWrite fromArray:audio];
		outframes += numFrames;
		
		if (ret <= 0)
            break;
	}
	
	//percent = 100;
	//[self performSelectorOnMainThread:@selector(updateBarOnMainThread:) withObject:self waitUntilDone:NO];
    
	DeallocateAudioBuffer(audio, numChannels);
	DiracDestroy( dirac );
	
	[_reader release];
	[_writer release];
	// Done!
	NSLog(@"\nDone!");
	//
	[self play];
	//[pool release];
	
	
}

- (void)play {
	NSLog(@"播放录制内容");
    NSError *error;
   // NSString *_filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithString:@"out.aif"]] retain];
    
    if (_player == NULL) {
        
    }else {
        [_player release];
    }
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:outUrl error:&error];
	_player.delegate = self;
    [_player play];
}


- (BOOL)canRecord {
    NSError *error;
    self.audioSession = [AVAudioSession sharedInstance];
    
    if (![self.audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error]) {
        NSLog(@"Error %@", [error localizedDescription]);
        return  NO;
    }
    
    if (![self.audioSession setActive:YES error:&error]) {
		NSLog(@"Error %@", [error localizedDescription]);
        return  NO;
    }
    
    return self.audioSession.inputIsAvailable;
}



/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	_lowTimes = 0;
	_playTimes = 0;
	
	
	//设置路径
    NSArray *searchPaths =  NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    audioPath = [[searchPaths objectAtIndex:0] stringByAppendingPathComponent:@"audio.wav"];
    outUrl = [[NSURL alloc] initFileURLWithPath:[[searchPaths objectAtIndex:0] stringByAppendingPathComponent:@"audio.aif"]];
	
    [audioPath retain];
	
	[self record];

    //启用监听
	[[SCListener sharedListener] listen];
	_timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateMeters:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
	
	UILabel *lbltitle = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
	[lbltitle setBackgroundColor:[UIColor yellowColor]];
	[lbltitle setText:@"学说话！"];
	[lbltitle setFont:[UIFont boldSystemFontOfSize:40.0f]];
	[lbltitle setTextAlignment:UITextAlignmentCenter];
	[lbltitle setTextColor:[UIColor greenColor]];
	[self.view addSubview:lbltitle];
	[lbltitle release];
	
	
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[_audioSession release];
    [_player release];
    [_recorder release];
	//[_reader release];
//	[_writer release];
	[_timer release];
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
	
    NSLog(@"录音结束并且成功  ～～～～～@@～～～～～～");
	//[self play];
	NSData *data = [NSData dataWithContentsOfURL:_recorder.url];
    [data writeToFile:audioPath atomically:YES];
	
	[self mackChangeSound];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error {
    NSLog(@"录音失败");
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    NSLog(@"播放结束！！！～～～@@～～～～");
	isPlaying = NO;
}

@end
