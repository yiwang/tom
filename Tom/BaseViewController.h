//
//  BaseViewController.h
//  Pokemon
//
//  Created by 威 王 on 12-7-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>
#import "SCListener.h"

@interface BaseViewController : UIViewController<AVAudioPlayerDelegate>{
    NSMutableArray *yawnArray;
    NSMutableArray *drinkArray;
    NSMutableArray *footLeftArray;
    NSMutableArray *footRightArray;
    NSMutableArray *stomachArray;
    NSMutableArray *knockout1Array;
    NSMutableArray *knockout2Array;
    NSMutableArray *scratchArray;
    NSMutableArray *cymbalArray;
    NSMutableArray *birdArray;
    NSMutableArray *pieArray;
    NSMutableArray *talkArray;
    
    NSURL *inUrl;
	NSURL *outUrl;
    
    NSURL *inUrlB;
	NSURL *outUrlB;
    AVAudioPlayer *player;
    UIImageView *gifImageView;
}

- (void)startYawn;
- (void)startListen;
- (void)stopListen;
- (void)playSoundWithFile:(NSString*)filename;
- (void)startAnimationWithImages:(NSMutableArray*)images duration:(NSTimeInterval)duration;
- (void)startAnimationWithImages:(NSMutableArray*)images duration:(NSTimeInterval)duration repeatCount:(int)repeatCount;
- (void)initGifImageView;
- (void)initDrinkArray;
- (void)initYawnArray;
- (void)initFootLeftArray;
- (void)initFootRightArray;
- (void)initStomachArray;
- (void)initKnockout1Array;
- (void)initKnockout2Array;
- (void)initScratchArray;
- (void)initCymbalArray;
- (void)initBirdArray;
- (void)initPieArray;
- (void)initTalkArray;
- (void)clearWithArray:(NSMutableArray *)array;

@end
